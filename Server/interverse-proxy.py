from flask import Flask,request,redirect
import json,requests,time
import simple_cache



cache = simple_cache.Cache()
print("Cache intitialized.")
app = Flask('interverse-proxy')

@app.route('/')
def index():
    return redirect("https://codeberg.org/gabe/Interverse",307)

@app.route("/initial", methods=['GET'])
def initial():
    url = request.args.get('url')
    if url == None:
        return redirect("https://codeberg.org/gabe/Interverse",307)
    data = cache.load_data(url)
    return json.dumps(data)

@app.route("/complete",methods=['GET'])
def complete():
    url = request.args.get('url')
    if url == None:
        return redirect("https://codeberg.org/gabe/Interverse",307)
    data = cache.get_interverse_data(url)
    return json.dumps(data)

if __name__ == '__main__':
    app.run()