# Interverse-proxy

A very simple flask server that caches interverse requests

## Endpoints:
Both require an `url` parameter   
`example.com` for example

`/inital`

Returns the data from a specified site.
Searches the following locations:

* `/.well-known/interverse`
* `/interverse.json`
* `/.well-known/discover.json` (for [Discover](https://codeberg.org/onasaft/Discover) compatibility)

    Has to change `preview_connections` into `connection_groups`

`/complete`

Returns initial in the `main` object then connection data in the `connections` object

## Running

The server can be run with [uwsgi](https://uwsgi-docs.readthedocs.io/en/latest/) with `start.sh`