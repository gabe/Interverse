# Running your own instance

The reference instance is running at [libresolutions.network](https://libresolutions.network/interverse)

If all your connections are adding the [CORS header](getting-started.md) all you need is to serve the content of [Client/index.html](../Client/index.html) from your page.

That file is written so that it can be easily pasted into an existing page, or used as a hugo shortcode.


Known instances:

* [jamespearson.xyz](https://jamespearson.xyz/interverse/)


## interverse-proxy

For compatibility with [Discover](https://codeberg.org/onasaft/Discover) or for displaying previews of sites not serving the CORS header you'll want to setup the [server](../Server/)

### Can I create my own client/Server?
**Please do!**
