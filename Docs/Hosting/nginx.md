# nginx configuration
```
location /.well-known/interverse {
    add_header 'Access-Control-Allow-Origin' '*';
    return 200,"{your data here...}"
    # or any other way of returning your file
}
```