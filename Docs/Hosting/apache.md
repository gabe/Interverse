# Apache

## .htaccess

```
<Location "/.well-known/interverse">
    <IfModule mod_headers.c>
        Header set Access-Control-Allow-Origin "*"
    </IfModule>
</Location>
```