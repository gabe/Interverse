# Getting started

## Create your interverse file

This file is essentially your 'listing'.

1. Grab the [example file](../interverse-example.json) or modify the [Libre Solutions Network](https://libresolutions.network/.well-known/interverse) one.
2. Make your changes, feel free to reference the [format](readme.md)
3. Ensure that the file is formatted as valid [JSON](https://www.json.org/json-en.html).  
Any errors will fail completely.

## Getting online

### Option A (ideal)
* You must serve valid JSON from `/.well-known/interverse`
* The header `access-control-allow-origin *` must also be [added](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

### Option B (simple)
* Serve valid JSON from either:
    
    * `/.well-known/interverse`
    * `/interverse.json`

Omitting the CORS header means that the client needs to ask a server to load the data, this can have performance impacts.

## How do I see my listing?

Interverse is a fully-decentralized system. You can [setup your instance]() or preview yourself at `https://libresolutions.network/interverse?url={your.domain}`. 

*Note: libresolutions.network uses a fairly long cache value when no results are returned.*  
 If you want to see your changes reflected immediately, consider adding the CORS header.

You can also run your own [instance](instance.md)