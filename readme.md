# Interverse



## A decentralized discovery service that allows you to easily discover sites and services.


* No complicated software to run (beyond an existing website)
* No Cryptocurrency/NFT
* Scalable
* Extensible 

![](media/interverse.gif)

[Try it out!](https://libresolutions.network/interverse)

[Demo video](https://libresolutions.network/videos/interverse-demo-1/)

### Protocol neutrality
Clients should be able to easily be ported to TOR/Gemini/IPFS/ect

### Can I join?
You don't need permission!
Simply follow these steps and you're online!

1. Check out [getting started](Docs/getting-started.md) to create your listing
2. You can also throw the [web client](Client/) on your webserver
3. If your connections require it the [interverse-proxy](Server/) is a useful fallback


If you need any help getting started feel free to get in touch at gabriel@libresolutions.network!

---
Now Compatible with [Discover](https://codeberg.org/onasaft/Discover)!


